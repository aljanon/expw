#!/bin/bash



parse_json_configuration() {
    local return_value=1
    if [[ -f "${EXPW_CONFIGURATION:-}" ]]; then
        log_info "Found JSON configuration file at %s" "$EXPW_CONFIGURATION"
        EXPW_CONFIGURATION="$(<${EXPW_CONFIGURATION})" \
            || { log_error "Could not read configuration file at %s" "$EXPW_CONFIGURATION"; return 1; }
    fi
    [[ -z "${EXPW_OUT_FOLDER:-}" ]] \
        && { EXPW_OUT_FOLDER="$(jq -r '.out_folder // empty' <<< ${EXPW_CONFIGURATION:-})" || return_value=1; }
    [[ -z "${EXPW_OUT_RESULTS:-}" ]] \
        && { EXPW_OUT_RESULTS_FILENAME="$(jq -r '.out_results // empty' <<< ${EXPW_CONFIGURATION:-})" || return_value=1; }
    [[ -z "${EXPW_PERF_EVENTS:-}" ]] \
        && { EXPW_PERF_EVENTS="$(jq -r '.perf_events // empty' <<< ${EXPW_CONFIGURATION:-})" || return_value=1; }
    [[ -z "${EXPW_CONSTANTS:-}" ]] \
        && { EXPW_CONSTANTS="$(jq -c '.constants // empty' <<< ${EXPW_CONFIGURATION:-})" || return_value=1; }
    [[ -z "${EXPW_VARIABLES:-}" ]] \
        && { EXPW_VARIABLES="$(jq -c '.variables // empty' <<< ${EXPW_CONFIGURATION:-})" || return_value=1; }
    return $return_value
}

prepare_env() {
    readonly CONFIG_TIMESTAMP="$(date +"%s.%N")"
    local return_value=0
    if [[ -n "${EXPW_CONFIGURATION:-}" ]]; then
        parse_json_configuration
        if parse_json_configuration; then
            log_info "Parsed JSON configuration"
        else
            log_error "Could not parse JSON configuration"
            return_value=1
        fi
    fi
    [[ -z "${EXPW_OUT_FOLDER:-}" ]] && EXPW_OUT_FOLDER="$EXPW_OUT_FOLDER_DEFAULT"
    [[ -z "${EXPW_OUT_RESULTS_FILENAME:-}" ]] && EXPW_OUT_RESULTS_FILENAME="${EXPW_OUT_RESULTS_FILENAME_DEFAULT}"
    [[ -z "${EXPW_OUT_RESULTS:-}" ]] && EXPW_OUT_RESULTS="${EXPW_OUT_FOLDER}/${EXPW_OUT_RESULTS_FILENAME}"
    [[ -n "${EXPW_PERF_EVENTS:-}" ]] && EXPW_PERF_EVENTS="${EXPW_PERF_EVENTS_DEFAULT},${EXPW_PERF_EVENTS}"
    [[ -z "${EXPW_PERF_EVENTS:-}" ]] && EXPW_PERF_EVENTS="${EXPW_PERF_EVENTS_DEFAULT}"
    [[ -z "${EXPW_CONSTANTS:-}" ]] && EXPW_CONSTANTS="[]"
    [[ -z "${EXPW_VARIABLES:-}" ]] && EXPW_VARIABLES="[]"
    [[ -z "${EXPW_OUT_TEMP:-}" ]] && EXPW_OUT_TEMP="${EXPW_OUT_FOLDER}/${CONFIG_TIMESTAMP}"
    [[ -z "${EXPW_OUT_TEMP_RESULTS:-}" ]] && EXPW_OUT_TEMP_RESULTS="${EXPW_OUT_TEMP}/${EXPW_OUT_RESULTS_FILENAME}"
    [[ -z "${EXPW_OUT_TEMP_PERF:-}" ]] && EXPW_OUT_TEMP_PERF="${EXPW_OUT_TEMP}/perf_temp.csv"
    EXPW_CONSTANTS="$(jq -cs '.[0] + .[1] | sort_by(.name)' <<< ""${EXPW_CONSTANTS}${EXPW_CONSTANTS_DEFAULT}"")" \
        || { log_error "Error while building constants array"; return_value=1; }
    EXPW_VARIABLES="$(jq -cs '.[0] + .[1] | sort_by(.name)' <<< ""${EXPW_VARIABLES}${EXPW_VARIABLES_DEFAULT}"")" \
        || { log_error "Error while building variables array"; return_value=1; }
    return $return_value
}

log_information() {
    log_info "Logging to %s" "$logfile"
    log_info "Experiment out folder: EXPW_OUT_FOLDER=%s" "$EXPW_OUT_FOLDER"
    log_info "Experiment temporary folder: EXPW_OUT_TEMP=%s" "$EXPW_OUT_TEMP"
    log_info "Experiment temporary results: EXPW_OUT_TEMP_RESULTS=%s" "$EXPW_OUT_TEMP_RESULTS"
    log_info "Experiment results file: EXPW_OUT_RESULTS=%s" "$EXPW_OUT_RESULTS"
    if [[ -z "${EXPW_PERF_EVENTS:-}" ]]; then
        log_info "perf disabled"
    else
        log_info "perf temporary file: EXPW_OUT_TEMP_PERF=%s" "$EXPW_OUT_TEMP_PERF"
        log_info "perf events: EXPW_PERF_EVENTS=%s" "$EXPW_PERF_EVENTS"
    fi
    log_info "Additional constants: EXPW_CONSTANTS=%s" "$EXPW_CONSTANTS"
    log_info "Additional variables: EXPW_VARIABLES=%s" "$EXPW_VARIABLES"
}

expw_configure() {
    local return_value=0
    check_dependencies "$EXPW_CONFIGURATION_DEPENDENCIES"
    verbosity=$INFO_LVL
    prepare_env \
        || { log_critical "Could not prepare environment. Aborting"; exit 1; }
    mkdir -p "$EXPW_OUT_FOLDER" \
        || { log_critical "Could not create out folder \`%s\`. Aborting." "$EXPW_OUT_FOLDER"; exit 1; }
    mkdir -p "$EXPW_OUT_TEMP" \
        || { log_critical "Could not create temporary folder \`%s\`. Aborting." "$EXPW_OUT_TEMP"; exit 1; }
    local -r script_name="$(basename ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})"
    logfile="${EXPW_OUT_TEMP}/${script_name}.log"
    touch "$logfile" \
        || { log_error "Could not create logfile at %s" "$logfile"; return_value=1; }
    verbosity=$CRIT_LVL
    log_information
    return $return_value
}

init_configuration() {
    local -r basedir="$(realpath $(dirname $BASH_SOURCE))"
    source "${basedir}/dependency.sh" || { printf "Could not source dependency check library. Aborting.\n"; exit 1; }
    source "${basedir}/log.sh" || { printf "Could not source log library. Aborting.\n"; exit 1; }
    declare -gr __EXPW_CONFIGURATION__=""
    declare -gr EXPW_CONFIGURATION_DEPENDENCIES="jq touch mkdir date"
    ## Default values
    declare -gr EXPW_OUT_FOLDER_DEFAULT="${HOME}/expw_out"
    declare -gr EXPW_OUT_RESULTS_FILENAME_DEFAULT="results.csv"
    declare -gr EXPW_PERF_EVENTS_DEFAULT="task-clock"
    declare -gr EXPW_VARIABLES_DEFAULT='[{"name":"timestamp","value":"date +%s.%N"}]'
    declare -gr EXPW_CONSTANTS_DEFAULT='[]'
    declare -gr EXPW_PERF_INTERVAL="1000000000"
}

{ [[ -z ${__EXPW_CONFIGURATION__+x} ]] && init_configuration; } || true
